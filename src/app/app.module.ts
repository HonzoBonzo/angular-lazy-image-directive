import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { AppComponent } from "./app.component";
import { ImageHolderComponent } from "./image-holder/image-holder.component";
import { LazyImageDirective } from './lazy-image/lazy-image.directive';

@NgModule({
  declarations: [AppComponent, ImageHolderComponent, LazyImageDirective],
  imports: [BrowserModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
