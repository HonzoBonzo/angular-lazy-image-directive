import { Directive, ElementRef, HostBinding, Input, OnInit, Renderer2 } from "@angular/core";
import { spinnerPlaceholderBase64, staticPlaceholderImageBase64 } from './lazy-image.config';

@Directive({
  selector: "[appLazyImage]"
})
export class LazyImageDirective implements OnInit {
  @HostBinding("attr.src") srcAttr = null;
  @Input() src: string;
  @Input() rootMargin = "200px";
  @Input() showLoader = true;
  @Input() showFade = true;
  @Input() fadeTransitionTime = "500ms";

  constructor(private el: ElementRef, private renderer: Renderer2) {}

  ngOnInit(): void {
    this.initConfig();
  }

  ngAfterViewInit() {
    this.canLazyLoad() ? this.lazyLoadImage() : this.loadImage();
  }

  private initConfig() {
    this.addPlaceholderImage();

    if (this.showFade) {
      this.addTransition();
      this.renderer.setStyle(this.el.nativeElement, "opacity", "0");
    }
  }

  private addTransition() {
    this.renderer.setStyle(
      this.el.nativeElement,
      "transition",
      `${this.fadeTransitionTime} ease-in-out`
    );
  }

  private addPlaceholderImage(): void {
    if (this.showLoader) {
      this.srcAttr = spinnerPlaceholderBase64;
    } else {
      this.srcAttr = staticPlaceholderImageBase64;
    }
  }

  private canLazyLoad() {
    return window && "IntersectionObserver" in window;
  }

  private lazyLoadImage() {
    const obs = new IntersectionObserver(
      entries => {
        entries.forEach(({ isIntersecting }) => {
          if (isIntersecting) {
            this.loadImage();
            obs.unobserve(this.el.nativeElement);
          }
        });
      },
      { rootMargin: this.rootMargin }
    );
    obs.observe(this.el.nativeElement);
  }

  private loadImage() {
    if (this.showFade) {
      this.renderer.setStyle(this.el.nativeElement, "opacity", "0");
    }

    this.srcAttr = this.src;

    if (this.showFade) {
      this.renderer.setStyle(this.el.nativeElement, "opacity", "1");
    }
  }
}
