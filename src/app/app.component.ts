import { Component, OnInit } from "@angular/core";
import { ImageInfo } from "./images/images.model";
import { ImagesService } from "./images/images.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
  images: ImageInfo[] = [];

  constructor(private imagesService: ImagesService) {}

  ngOnInit(): void {
    this.fetchImages();
  }

  fetchImages(): void {
    this.imagesService
      .getImages()
      .subscribe(response => (this.images = response));
  }

  trackById(_index: number, image: ImageInfo): number {
    return image.id;
  }
}
