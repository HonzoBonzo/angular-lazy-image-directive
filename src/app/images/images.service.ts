import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from 'rxjs';
import { catchError, timeout } from 'rxjs/operators';
import { ImageInfo, mockImages } from "./images.model";

@Injectable({
  providedIn: "root"
})
export class ImagesService {
  apiUrl = "https://jsonplaceholder.typicode.com";

  constructor(private httpClient: HttpClient) {}

  getImages(): Observable<ImageInfo[]> {
    return this.httpClient
      .get<ImageInfo[]>(`${this.apiUrl}/albums/10/photos?_limit=50`)
      .pipe(
        timeout(5000),
        catchError(() => of(mockImages))
      )
  }
}
