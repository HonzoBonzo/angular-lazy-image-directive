import { Component, Input, OnInit } from '@angular/core';
import { ImageInfo } from '../images/images.model';

@Component({
  selector: 'app-image-holder',
  templateUrl: './image-holder.component.html',
  styleUrls: ['./image-holder.component.scss']
})
export class ImageHolderComponent implements OnInit {
  @Input() imageInfo: ImageInfo;
  showFade: boolean;
  readyToShow = false;

  ngOnInit(): void {
    this.showFade = this.getRandomBoolean();
    this.readyToShow = true;
  }

  getRandomBoolean(): boolean {
    return Math.random() < 0.5;
  }
}
